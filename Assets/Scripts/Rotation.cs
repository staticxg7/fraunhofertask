using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    //speed control for rotation in individual axis
    public float rotationSpeedX;
    public float rotationSpeedY;
    public float rotationSpeedZ;

    private bool rotateEnabled = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (rotateEnabled == true)
        {
            //updating rotation every frame of gameobject
            transform.Rotate(new Vector3(rotationSpeedX,rotationSpeedY,rotationSpeedZ) * Time.deltaTime);
        }

    }

    public void stopRotate()
    {
        //function to stop rotation of gameobject
        rotateEnabled = false;
        transform.Rotate(new Vector3(0,0,0)*Time.deltaTime);

    }

    public void IncreaseSpeed()
    {
        //function to increase speed of gameobject in all axis equally
        rotateEnabled = true;
        rotationSpeedX += 5;
        rotationSpeedY += 5;
        rotationSpeedZ += 5;

    }

    public void decreaseSpeed()
    {
        //function to decrease speed of gameobject in all axis equally
        rotateEnabled = true;
        rotationSpeedX -= 5;
        rotationSpeedY -= 5;
        rotationSpeedZ -= 5;

    }
}
