using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class UIdoc : MonoBehaviour
{
    public Rotation gameobj;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        VisualElement root = GetComponent<UIDocument>().rootVisualElement;

        Button increase = root.Q<Button>("IncreaseSpeed");
        Button decrease = root.Q<Button>("DecreaseSpeed");
        Button stop = root.Q<Button>("RotationStop");

        increase.clicked += () => gameobj.IncreaseSpeed();
        decrease.clicked += () => gameobj.decreaseSpeed();
        stop.clicked += () => gameobj.stopRotate();
    }
}
