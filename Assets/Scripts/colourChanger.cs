using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class colourChanger : MonoBehaviour
{
    //inititalising variables
    private Renderer render;
    // Start is called before the first frame update
    void Start()
    {
        //assigning mesh renderer to variable
        render = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        //Checking for left mouse button clicks
        if (Input.GetMouseButton(0))
        {
            //initialising raycast variable
            RaycastHit hit;
            //making sure the mouse pointer is hovering on object
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            //checking if there is a raycast hit on the gameobject
            if (Physics.Raycast(ray, out hit) && hit.collider == GetComponent<Collider>())
            {
                //assigning random colours to mesh renderer(color attribute property)
                render.material.color = Random.ColorHSV();
            }
        }
        
    }
}
